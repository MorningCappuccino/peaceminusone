<?php

 $type = $_GET["type"];

$sorting = $_GET["sort"];

switch ($sorting)
{
    case 'price-asc';
    $sorting = 'price ASC';
    $sort_name = 'От дешевых к дорогим';
    break;

    case 'price-desc';
    $sorting = 'price DESC';
    $sort_name = 'От дорогих к дешевым';
    break;

    case 'popular';
    $sorting = 'count DESC';
    $sort_name = 'Популярное';
    break;

    case 'news';
    $sorting = 'datetime DESC';
    $sort_name = 'Новинки';
    break;

    case 'brand';
    $sorting = 'nazv';
    $sort_name = 'От А до Z';
    break;

    default:
    $sorting = 'product_id ASC';
    $sort_name = 'Нет сортировки';
    break;
}
?>

<head>
	<meta charset="utf-8" />

	<title>PEACEMINUSONE</title>
	<link rel="shortcut icon" href="../img/logo.jpg" />
	<link rel="stylesheet" href="libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
	<link rel="stylesheet" href="libs/countdown/jquery.countdown.css" />
	<link rel="stylesheet" href="css/fonts.css" />
	<link rel="stylesheet" href="css/main.css" />
	<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/pmoo.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
</head>
<?php
require ("header.php");
require ("db.php");
?>
<section class="for-left-menu">
<div id="left-menu">
	<p>Производитель:</p>
	<ul>

		<li><a id="sk">Южная Корея</a>
			<ul class="brand-section">
			<?php

			$res = mysqli_query($link,"SELECT * FROM brand WHERE Country='Южная Корея'");
			if(mysqli_num_rows($res)>0)
		{ $row = mysqli_fetch_array($res);
			do {
				echo '<li><a href="brand.php?type='.$row[nazv].'">'.$row[nazv].'</a></li>';
			 }
			 while ($row = mysqli_fetch_array($res));
			}

		?>
			</ul>
		</li>

		<li><a id="thai">Таиланд</a>
			<ul class="brand-section">
			<?php
		$res = mysqli_query($link,"SELECT * FROM brand WHERE Country='Таиланд'");
			if(mysqli_num_rows($res)>0)
		{ $row = mysqli_fetch_array($res);
			do {
				echo '<li><a href="brand.php?type='.$row[nazv].'">'.$row[nazv].'</a></li>';
			 }
			 while ($row = mysqli_fetch_array($res));
			}
?>
			</ul>
		</li>
		<li><a href="brand.php">Все товары</a></li>
	</ul>
</div>

<div id="block-sorting">
	<ul id="options-list">
		<li><p>Сортировать:</p></li>
		<li><a id="select-sort"><?php echo $sort_name; ?></a>
			<ul id="sorting-list">
				<li><a href="brand.php?type=<? $type ?>&sort=price-asc&page=<?$page?>" >От дешевых к дорогим</a></li>
				<li><a href="brand.php?type=<? $type ?>&sort=price-desc&page=<?$page?>" >От дорогих к дешевым</a></li>
				<li><a href="brand.php?type=<? $type ?>&sort=popular&page=<?$page?>" >Популярное</a></li>
				<li><a href="brand.php?type=<? $type ?>&sort=news&page=<?$page?>" >Новинки</a></li>
				<li><a href="brand.php?type=<? $type ?>&sort=brand&page=<?$page?>" >От А до Z</a></li>
			</ul>
		</li>
	</ul>
</div>
</section>
<section class="all-tov">

<div>
	<?php
if (!empty($type))
    {
       $querycat = "WHERE nazv='$type' AND visible='1'";
    }else
    {
       $querycat = "";
    }

    $num = 40; // указ на количество вывод товаров.
    $page = (int)$_GET['page'];

    $count = mysqli_query($link,"SELECT COUNT(*) FROM product $querycat ");
    $temp = mysqli_fetch_array($count);

    If ($temp[0] > 0)
    {
    $tempcount = $temp[0];

    // общие число страниц
    $total = (($tempcount - 1) / $num) + 1;
    $total =  intval($total);

    $page = intval($page);

    if(empty($page) or $page < 0) $page = 1;

    if($page > $total) $page = $total;

    // с какого товара будет выводится товары
    $start = $page * $num - $num;

    $qury_start_num = " LIMIT $start, $num";
    }




		$res = mysqli_query($link,"SELECT * FROM product $querycat ORDER BY $sorting $qury_start_num ");

			if(mysqli_num_rows($res)>0)
		{ $row = mysqli_fetch_array($res);
			do {
				echo ('
			<div class="cont">
				<div><img class="cont_img" src="../images/bd/'.$row[image].'"></div>
				<div class="cont_link"><a  href="view_content.php?id='.$row[product_id].'">'.$row[title].'</a></div>
				<p class="op">'.$row[price].' BYN</p>
			</div> ');

		}
		while ($row = mysqli_fetch_array($res));
	}

	if ($page != 1){ $pstr_prev = '<li><a class="pstr-prev" href="brand.php?'.$catlink.'type='.$type.'&page='.($page - 1).'">&lt;</a></li>';}
if ($page != $total) $pstr_next = '<li><a class="pstr-next" href="brand.php?'.$catlink.'type='.$type.'&page='.($page + 1).'">&gt;</a></li>';


// Формируем ссылки со страницами
if($page - 5 > 0) $page5left = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page - 5).'">'.($page - 5).'</a></li>';
if($page - 4 > 0) $page4left = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page - 4).'">'.($page - 4).'</a></li>';
if($page - 3 > 0) $page3left = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page - 3).'">'.($page - 3).'</a></li>';
if($page - 2 > 0) $page2left = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page - 2).'">'.($page - 2).'</a></li>';
if($page - 1 > 0) $page1left = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page - 1).'">'.($page - 1).'</a></li>';

if($page + 5 <= $total) $page5right = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page + 5).'">'.($page + 5).'</a></li>';
if($page + 4 <= $total) $page4right = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page + 4).'">'.($page + 4).'</a></li>';
if($page + 3 <= $total) $page3right = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page + 3).'">'.($page + 3).'</a></li>';
if($page + 2 <= $total) $page2right = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page + 2).'">'.($page + 2).'</a></li>';
if($page + 1 <= $total) $page1right = '<li><a href="brand.php?'.$catlink.'type='.$type.'&page='.($page + 1).'">'.($page + 1).'</a></li>';

if ($page+5 < $total)
{
    $strtotal = '<li><p class="nav-point">..</p></li><li><a href="brand.php?'.$catlink.'type='.$type.'&page='.$total.'">'.$total.'</a></li>';
}else
{
    $strtotal = "";
}

if ($total > 1)
{
    echo '
    <div class="pstrnav">
    <ul>
    ';
    echo $pstr_prev.$page5left.$page4left.$page3left.$page2left.$page1left."<li><a class='pstr-active' href='brand.php?'.$catlink.'type='.$type.'sort='.$sorting.'page=".$page."'>".$page."</a></li>".$page1right.$page2right.$page3right.$page4right.$page5right.$strtotal.$pstr_next;
    echo '
    </ul>
    </div>
    ';
}﻿
?>
</div>
</section>






<?php
require ("footer.php"); ?>
