<head>
	<meta charset="utf-8" />

	<title>PEACEMINUSONE</title>
	<link rel="shortcut icon" href="../img/logo.jpg" />
	<link rel="stylesheet" href="libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
	<link rel="stylesheet" href="libs/countdown/jquery.countdown.css" />
	<link rel="stylesheet" href="css/fonts.css" />
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/about.css" />
	<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/pmoo.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
</head>
<?php
require ("header.php");
require ("db.php");
 ?>
<section>
		<main id="content">
			<h1 class="home">Интернет-магазин корейской косметики в Минске</h1>
				<hr>
					<div class="home-section" style="line-height: 1.4;">
						<p style="text-align: justify;">
				Добро пожаловать в минский интернет магазин корейской косметики LOLA.BY. Хотите в свои 30 лет выглядеть на 20, а в 40 лет на 30 - тогда Вы на правильном пути. Используйте корейские косметические средства. Корейские патчи, сыворотки и крема для лица. Шампуни, кондиционеры и филеры для волос. Гели, скрабы и пилинги для тела.
						</p>

					<div class="home_cat">
						<a href="katalog">
							<p class="naz"><b>Весь каталог</b></p>
								<p class="im"><img loading="lazy" src="https://lola.by/image/cache/home_menu/all-220x220.jpg" class=""></p>
						</a>
						<a href="korean-creams-face">
							<p class="naz"><b>Крема для лица</b></p>
								<p class="im"><img loading="lazy" src="https://lola.by/image/cache/home_menu/korean-creams-face-220x220.jpg" class=""></p>
						</a>
						<a href="korean-masks-face">
							<p class="naz"><b>Маски для лица</b></p>
								<p class="im"><img loading="lazy" src="https://lola.by/image/cache/home_menu/korean-masks-face-220x220.jpg" class=""></p>
						</a>
						<a href="korean-serums-face">
							<p class="naz"><b>Сыворотки для лица</b></p>
								<p class="im"><img loading="lazy" src="https://lola.by/image/cache/home_menu/korean-serums-face-220x220.jpg" class=""></p>
						</a>
						<a href="korean-patches">
							<p class="naz"><b>Патчи для глаз</b></p>
								<p class="im"><img loading="lazy" src="https://lola.by/image/cache/home_menu/korean-patches-220x220.jpg" class=""></p>
						</a>
					</div>

			<h2>Особенности корейской косметики</h2>
				<hr>
					<p style="text-align: justify;">
				Хайп вокруг корейской косметики вызван уникальностью свойств и высоко технологическим составом корейской продукции. Муцин улитки, гиалуроновая кислота, змеиный пептид, коллаген и другие натуральные компоненты, которые используются в профессиональной косметике. Гипоаллергенная, без токсичных фталатов и формальдегида, проверено в korea lab. В основе натуральные лечебные компоненты и в уходовых, и в декоративных линейках. Оригинальные экстракты, эссенции, вытяжки, масла. Кореянки используют только самую лучшую косметику.
					</p>
					<p style="text-align: justify;">В интернет магазине лола бай покупают натуральную корейскую косметику в онлайн рассрочку по картам ХАЛВА и КАРТА ПОКУПОК. Заказы доставляются в любой город Беларуси: Минск, Витебск, Могилёв, Гомель, Брест, Гродно. При заказе косметики через сайт используйте промокоды на разовую скидку или регистрируйте личный кабинет и получайте накопительную скидку до 20%. Хотите сделать оригинальный подарок близким - подарочный сертификат шикарный вариант.</p>
				</div>
	<div class="home-section">
		<div class="home-part part1">
				<h2>Преимущества LOLA.BY</h2>
				<hr>
				<table>
					<tbody><tr>
						<td colspan="2">
							<p style="text-align: justify;">Популярность корейской косметики стремительно растёт в Беларуси. Магазины открываются в каждом городе РБ. В каждом торговом центре Минска работает как минимум островок косметики из Кореи. Количество белорусских интернет магазинов косметики зашкаливает и уже взлетело до небес. Поэтому важно выделяться из серой массы. Мы делаем настоящий asiashop и это у сайта лола бай получается.</p>
						</td>
					</tr>
					<tr>
						<td class="w1"></td>
						<td>
							<p><b>Подарки при каждом заказе</b></p>
							<p>Независимо от размера Вашего заказа в каждой посылке Вы найдёте небольшой, но приятный сюприз</p>
						</td>
					</tr>
					<tr>
						<td class="w2"></td>
						<td>
							<p><b>Огромный выбор товаров</b></p>
							<p>Наш ассортимент <b>3283 товаров</b> и <b>125 брендов</b> в каталоге. У нас представлены практически все популярные производители корейской и тайской косметики.</p>
						</td>
					</tr>
					<tr>
						<td class="w3"></td>
						<td>
							<p><b>Высокое качество</b></p>
							<p>Мы заказываем товары у производителей в Корее. Это дает вам уверенность в том, что вся продукция оригинальная.</p>
						</td>
					</tr>
					<tr>
						<td class="w4"></td>
						<td>
							<p><b>Доверие клиентов</b></p>
							<p>Несколько десятков тысяч доставленных товаров и <b>14167 счастливых покупателей</b>. Реальные покупатели оставили <b>1297 отзывов</b> о наших товарах.</p>
						</td>
					</tr>
					<tr>
						<td class="w5"></td>
						<td>
							<p><b>Доставка по Беларуси</b></p>
							<p>Мы доставляем заказы по всех городам Беларуси: Минск, <a style="color: #333;" href="mogilev">Могилёв</a>, Гомель, Витебск, Брест, Гродно, Бобруйск, Барановичи, Борисов, Пинск, Орша, Мозырь, Солигорск, Новополоцк, Лида -  максимально быстро. Бесплатно при заказе от 45 руб.</p>
						</td>
					</tr>
				</tbody></table>
			</div>
			<div class="home-part">
				<h2>Отзывы о корейской косметике</h2>
				<span class="otz_stat" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
					<span><a href="reviews-site">Отзывы</a>: <b itemprop="reviewCount">52</b></span>
					<span>Рейтинг: <b itemprop="ratingValue">5.00</b></span>
				</span>

				<hr>
				<table class="otziv">
					<tbody><tr>
						<td colspan="2">
							<p style="text-align: justify;margin-bottom: 10px;">Хвалить себя самого - дело спорное. Намного лучше когда хвалят другие. Объективные эксперты. Те, кто не раз испытал на себе плюсы, а возможно и минусы взаимодействия с магазином lola.by. Предлагаем подборку из нескольких последних отзывов, оставленных покупателями о сайте.</p>
						</td>
					</tr>
					<tr>
						<td>
							<div class="stars"><div class="on" style="width: 100%;"></div></div>
							<p><b>Alice Vinchenko</b></p>
							<p class="ital">"Выражаю благодарность девушке – консультанту за совет в выборе сыворотки. Бесподобная сыворотка, для моей проблемной кожи подошла идеально.  За корейским уходом теперь только к вам)"</p>
						</td>
					</tr>
					<tr>
						<td>
							<div class="stars"><div class="on" style="width: 100%;"></div></div>
							<p><b>Наталья</b></p>
							<p class="ital">"Огромное спасибо за оперативную доставку заказа и приятный презент в посылочке. Раньше заказывала в korealab, но на вашем сайте намного удобнее. Обязательно обращусь к вам и в следующий раз!"</p>
						</td>
					</tr>
					<tr>
						<td>
							<div class="stars"><div class="on" style="width: 100%;"></div></div>
							<p><b>Кристина Белоновская</b></p>
							<p class="ital">"Спасибо за совет в выборе шампуня и кондиционера! Отлично мне подошли, волосы легкие и шелковисты после применения.  И ,что очень важно, сроки продукции отличные, потому что такого объема мне хватит надолго."</p>
						</td>
					</tr>
					<tr>
						<td>
							<div class="stars"><div class="on" style="width: 100%;"></div></div>
							<p><b>Анюта К</b></p>
							<p class="ital">"Отличные цены, дешевле чем на allcosmetics, качественная консультация и быстрая доставка продукции. Заказ был очень хорошо упакован.  Буду  приобретать корейскую косметику теперь только у  Вас!"</p>
						</td>
					</tr>
					<tr>
						<td>
							<div class="stars"><div class="on" style="width: 100%;"></div></div>
							<p><b>Валентина</b></p>
							<p class="ital">"Очень хороший магазин, а самое главное приятный интерфейс. После оформление заказа, очень оперативно позвонили и подтвердили заказ. И уже  через 2 дня посылка была  у меня. В коробке с заказом, меня ждал приятный сюрприз!"</p>
						</td>
					</tr>
				</tbody></table>
			</div>
			</div>

	</main>
</section>
<?php
require ("footer.php"); ?>