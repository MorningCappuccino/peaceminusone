<?php
session_start();


$sorting = $_GET["sort"];
 $id = $_GET["id"];


?>
<!DOCTYPE html>
<html>
<head>
  <title>PEACEMINUSONE</title>
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="shortcut icon" href="../img/logo.jpg" />
    <link rel="stylesheet" href="libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
  <link rel="stylesheet" href="libs/countdown/jquery.countdown.css" />
  <link rel="stylesheet" href="css/fonts.css" />
  <link rel="stylesheet" href="css/main.css" />
  <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="js/pmoo.js"></script>
  <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
  <script type="text/javascript" src="js/jTabs.js"></script>
  <link rel="stylesheet" type="text/css" href="/fancybox/jquery.fancybox.css" />
  <script type="text/javascript" src="/fancybox/jquery.fancybox.js"></script>


</head>
 <script type="text/javascript">
$(document).ready(function(){

    $("ul.tabs").jTabs({content: ".tabs_content", animate: true, effect:"fade"});
    /*$(".image-modal").fancybox();*/
    $(".send-review").fancybox();


});
</script>
<body>


<div id="block-body">

<?php
require ("header.php");
require ("db.php");
?>

<section id="block-content">
<?php
$result1 = mysqli_query($link, "SELECT * FROM product WHERE product_id='$id' AND visible='1'");
If (mysqli_num_rows($result1) > 0)
{
$row1 = mysqli_fetch_array($result1);
do
{
if  (strlen($row1["image"]) > 0 && file_exists("./images/bd/".$row1["image"]))
{
$img_path = './images/bd/'.$row1["image"];
$max_width = 480;
$max_height = 480;
 list($width, $height) = getimagesize($img_path);
$ratioh = $max_height/$height;
$ratiow = $max_width/$width;
$ratio = min($ratioh, $ratiow);

$width = intval($ratio*$width);
$height = intval($ratio*$height);
}else
{
$img_path = "/images/bd/no_image.png";
$width = 480;
$height = 480;
}

// Количество отзывов
// $query_reviews = mysqli_query($link, "SELECT * FROM table_reviews WHERE products_id = '$id' AND moderat='1'" );

// $count_reviews = mysqli_num_rows($query_reviews);


echo  '


<div id="block-content-info">

<img src="'.$img_path.'" width="'.$width.'" height="'.$height.'" />

<div id="block-mini-description">

<p id="content-title">'.$row1["title"].'</p>


<p id="style-price" >'.$row1["price"].' BYN</p>

<p id="style-brand"><img src="/img/'.$row1["img"].'"></p>
<br>
<p id="content-text">'.$row1["mini_description"].'</p>



</div>

</div>

';


}


 while ($row1 = mysqli_fetch_array($result1));




$result = mysqli_query($link, "SELECT * FROM product WHERE product_id='$id' AND visible='1'");
$row = mysqli_fetch_array($result);

echo '
<ul class="tabs">
    <li><a class="active" href="#" >Описание</a></li>
    <li><a href="#" >Отзывы</a></li>
</ul>

<div class="tabs_content">

<div>'.$row["description"].' <br> <img src="/images/bd/'.$row["image"].'"/></div>

<div>
<p id="link-send-review" ><a class="send-review" href="#send-review" >Написать отзыв</a></p>
';


$query_reviews = mysqli_query($link, "SELECT * FROM table_reviews WHERE product_id='$id' AND moderat='1' ORDER BY reviews_id DESC" );

if (mysqli_num_rows($query_reviews) > 0)
{
$row_reviews = mysqli_fetch_array($query_reviews);
do
{

echo '
<div class="block-reviews" >
<img id="comm" src="/images/bd/no_image.png"/>
<p class="author-date" ><strong>'.$row_reviews["name"].'</strong>,<br>'.$row_reviews["data"].'</p><br><br>
<img src="/images/plus-reviews.png"/>
<p class="textrev" >'.$row_reviews["good_reviews"].'</p>
<img src="/images/minus-reviews.png"/>
<p class="textrev" >'.$row_reviews["bad_reviews"].'</p>

<p class="text-comment">'.$row_reviews["comment"].'</p>
</div>

';

}
 while ($row_reviews = mysqli_fetch_array($query_reviews));
}

else
{
    echo '<p class="title-no-info" >Отзывов нет</p>';
}



echo '
</div>

</div>
<form method="POST">
  <div id="send-review" >

    <p align="right" id="title-review">Публикация отзыва производится после предварительной модерации.</p>

    <ul>
   <li><p align="right"><label id="label-name" >Имя<span>*</span></label><input maxlength="15" type="text"  id="name_review" /></p></li>
    <li><p align="right"><label id="label-good" >Достоинства<span>*</span></label><textarea id="good_review" ></textarea></p></li>
    <li><p align="right"><label id="label-bad" >Недостатки<span>*</span></label><textarea id="bad_review" ></textarea></p></li>
    <li><p align="right"><label id="label-comment" >Комментарий</label><textarea id="comment_review" ></textarea></p></li>


    </ul>
    <p id="reload-img"><img src="/images/loading.gif"/></p> <p id="button-send-review" iid="'.$id.'" ></p>

    </div>
</form>

';

}




?>
 <script type="text/javascript">
$(document).ready(function(){

$('#button-send-review').click(function(){

   var name = $("#name_review").val();
   var good = $("#good_review").val();
   var bad = $("#bad_review").val();
   var comment = $("#comment_review").val();
   var iid = $("#button-send-review").attr("iid");

    if (name != "")
     {
          name_review = '1';
          $("#name_review").css("borderColor","#DBDBDB");
      }else {
           name_review = '0';
           $("#name_review").css("borderColor","#FDB6B6");
      }

    if (good != "")
       {
          good_review = '1';
          $("#good_review").css("borderColor","#DBDBDB");
      }else {
          good_review = '0';
          $("#good_review").css("borderColor","#FDB6B6");
      }

    if (bad != "")
     {
          bad_review = '1';
          $("#bad_review").css("borderColor","#DBDBDB");
     }else {
          bad_review = '0';
          $("#bad_review").css("borderColor","#FDB6B6");
     }


            // Глобальная проверка и отправка отзыва



      $.ajax({
         type: "POST",
          url: "view_content.php",
         data: "id="+iid+"&name="+name+"&good="+good+"&bad="+bad+"&comment="+comment,
         dataType: "html",
         cache: false,
         success: function() {
         setTimeout("$.fancybox.close()", 1000);
         }
         });


});
  });
</script>
<?php



if($_SERVER["REQUEST_METHOD"] == "POST")
{

 include("db.php");

if(!empty($_POST)){
  $id=  $_POST['id'];
 $name =  $_POST['name'];
 $good = $_POST['good'];
 $bad =  $_POST['bad'];
 $comment =  $_POST['comment'];
 $date = date("Y-m-d");



        mysqli_query($link,"INSERT INTO table_reviews(product_id,name,good_reviews,bad_reviews,comment,data)
            VALUES('".$id."','".$name."','".$good."','".$bad."','".$comment."','".$date."')");
}
}
?>
</section>

<?php
require ("footer.php");
?>

</div>

</body>
</html>