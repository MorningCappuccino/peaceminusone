<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<title>PEACEMINUSONE</title>
	<meta content="" name="description" />
	<meta content="" property="og:image" />
	<meta content="" property="og:description" />
	<meta content="" property="og:site_name" />
	<meta content="website" property="og:type" />

	<meta content="telephone=no" name="format-detection" />
	<meta http-equiv="x-rim-auto-match" content="none">

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="shortcut icon" href="../img/logo.jpg" />

	<link rel="stylesheet" href="libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
	<link rel="stylesheet" href="libs/font-awesome-4.2.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="libs/fancybox/jquery.fancybox.css" />
	<link rel="stylesheet" href="libs/owl-carousel/owl.carousel.css" />
	<link rel="stylesheet" href="libs/countdown/jquery.countdown.css" />
	<link rel="stylesheet" href="css/fonts.css" />
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/media.css" />
</head>
<body>

<?php
require ("header.php"); ?>
<section class="fortov">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Каталог Азиатской косметики</h2>
				<div class="home_cat">
							<a href="brand.php">
								<p class="naz">
									<b>Весь каталог</b>
								</p>
								<p class="im">
									<img loading="lazy" src="img/vk.jpg" alt="">
								</p>
							</a>
							<a href="cream.php">
								<p class="naz">
									<b>Крема для лица</b>
								</p>
								<p class="im">
									<img loading="lazy" src="img/kc.jpg" alt="">
								</p>
							</a>
							<a href="mask.php">
								<p class="naz">
									<b>Маски для лица</b>
								</p>
								<p class="im">
									<img loading="lazy" src="img/km.jpg" alt="">
								</p>
							</a>
							<a href="serum.php">
								<p class="naz">
									<b>Сыворотки для лица</b>
								</p>
								<p class="im">
									<img loading="lazy" src="img/ks.jpg" alt="">
								</p>
							</a>
							<a href="patches.php">
								<p class="naz">
									<b>Патчи для глаз</b>
								</p>
								<p class="im">
									<img loading="lazy" src="img/kp.jpg" alt="">
								</p>
							</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section  class="fortov b">
		<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Популярные бренды</h2>
				<div class="br">
					<ul>
						<li><a href="brand.php?type=A`PIEU" class="brand"><img src="img/apieu.jpg" alt="Alt"><p id="b">A'PIEU</p></a></li>
						<li><a href="brand.php?type=ELIZAVECCA" class="brand"><img src="img/elizavecca.jpg" alt="Alt"><p id="b">ELIZAVECCA</p></a></li>
						<li><a href="brand.php?type=LEBELAGE" class="brand"><img src="img/lebelage.jpg" alt="Alt"><p id="b">LEBELAGE</p></a></li>
						<li><a href="brand.php?type=MIZON" class="brand"><img src="img/mizon.jpg" alt="Alt"><p id="b">MIZON</p></a></li>
						<li><a href="brand.php?type=PRRETI" class="brand"><img src="img/prreti.jpg" alt="Alt"><p id="b">PRRETI</p></a></li>
						<li><a href="brand.php?type=SEANTREE" class="brand"><img src="img/seantree.jpg" alt="Alt"><p id="b">SEANTREE</p></a></li>
						<li><a href="brand.php?type=KUAN IM" class="brand"><img src="img/kuan-im.jpg" alt="Alt"><p id="b">KUAN IM</p></a></li>
						<li><a href="brand.php?type=HELLO BEAUTY" class="brand"><img src="img/hello-beauty.jpg" alt="Alt"><p id="b">HELLO BEAUTY</p></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
require ("footer.php"); ?>
	<!-- <div class="ul2">
		<ul >
			<li><a href="#">Китай</a></li>
			<li><a href="#">Тайланд</a></li>
			<li><a href="#">Южная Корея</a></li>
			<li><a href="#">Япония</a></li>
		</ul>
	</div> -->
	<div class="hidden"></div>
	<!-- Mandatory for Responsive Bootstrap Toolkit to operate -->
	<div class="device-xs visible-xs"></div>
	<div class="device-sm visible-sm"></div>
	<div class="device-md visible-md"></div>
	<div class="device-lg visible-lg"></div>
	<!-- end mandatory -->
	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->
	<script src="libs/jquery/jquery-1.11.1.min.js"></script>
	<script src="libs/jquery-mousewheel/jquery.mousewheel.min.js"></script>
	<script src="libs/fancybox/jquery.fancybox.pack.js"></script>
	<script src="libs/waypoints/waypoints-1.6.2.min.js"></script>
	<script src="libs/scrollto/jquery.scrollTo.min.js"></script>
	<script src="libs/owl-carousel/owl.carousel.min.js"></script>
	<script src="libs/countdown/jquery.plugin.js"></script>
	<script src="libs/countdown/jquery.countdown.min.js"></script>
	<script src="libs/countdown/jquery.countdown-ru.js"></script>
	<script src="libs/landing-nav/navigation.js"></script>
	<script src="libs/bootstrap-toolkit/bootstrap-toolkit.min.js"></script>
	<script src="libs/maskedinput/jquery.maskedinput.min.js"></script>
	<script src="libs/equalheight/jquery.equalheight.js"></script>
	<script src="libs/stellar/jquery.stellar.min.js"></script>
	<script src="js/common.js"></script>
 		<!-- карты гугл -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<link href="http://yandex.st/jquery/fancybox/2.1.4/jquery.fancybox.css" rel="stylesheet" />
<script src="http://yandex.st/jquery/fancybox/2.1.4/jquery.fancybox.min.js"></script>
	<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter --><!-- /Google Analytics counter -->
</body>
</html>