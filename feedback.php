<?php

   include("functions/functions.php");
session_start();


if ($_POST["send_message"])
{
    $error = array();

  if (!$_POST["feed_name"]) $error[] = "Укажите своё имя";

  if(!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i",trim($_POST["feed_email"])))
  {
    $error[] = "Укажите корректный E-mail";
  }

  if (!$_POST["feed_subject"]) $error[] = "Укажите тему письма!";
  if (!$_POST["feed_text"]) $error[] = "Укажите текст сообщения!";



  /*if (strtolower($_POST["reg_captcha"]) != $_SESSION['img_captcha'])
  {
    $error[] = "Неверный код с картинки!";
  }*/


   if (count($error))
   {
     $_SESSION['message'] = "<p id='form-error'>".implode('<br />',$error)."</p>";

   }else
   {
    	         send_mail($_POST["feed_email"],
						       'takinagaaoikan@yandex.ru',
						$_POST["feed_subject"],
						'От: '.$_POST["feed_name"].'<br/>'.$_POST["feed_text"]);

     $_SESSION['message'] = "<p id='form-success'>Ваше сообщение успешно отправлено!</p>";

   }

}
?>
<!DOCTYPE html>
<html>
<head>
 <title>PEACEMINUSONE</title>
  <link rel="shortcut icon" href="../img/logo.jpg" />
  <link rel="stylesheet" href="libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
  <link rel="stylesheet" href="libs/countdown/jquery.countdown.css" />
  <link rel="stylesheet" href="css/fonts.css" />
  <link rel="stylesheet" href="css/main.css" />
  <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/pmoo.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="js/jquery.form.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
</head>
<body>

<?php
   require("header.php");
?>

<section id="block-contents">

<form method="post">
<div id="block-feedback">
<ul id="feedback">
<?php

   if(isset($_SESSION['message']))
  {
  echo $_SESSION['message'];
  unset($_SESSION['message']);
  }
?>

<li><label>Ваше Имя</label><input type="text" name="feed_name"  /></li>
<li><label>Ваш E-mail</label><input type="text" name="feed_email"  /></li>
<li><label>Тема</label><input type="text" name="feed_subject"  /></li>
<li><label>Текст сообщения</label><textarea name="feed_text" ></textarea></li>


<!-- <li>
<label for="reg_captcha">Защитный код</label>
<div id="block-captcha">
<img src="reg_captcha.php" />
<input type="text" name="reg_captcha" id="reg_captcha" />
<p id="reloadcaptcha">Обновить</p>
</div>
</li> -->

</ul>
<p id="contacts" align="center"><input type="submit" name="send_message" id="form_submit" /></p>
</div>

</form>

</section>

<?php
    require("footer.php");
?>


</body>
</html>
