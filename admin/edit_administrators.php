<?php
session_start();
if ($_SESSION['auth_admin'] == "yes_auth")
{

       if (isset($_GET["logout"]))
    {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }
  $_SESSION['urlpage'] = "<a href='index.php' >Главная</a> \ <a href='edit_administrators.php' >Изменение администратора</a>";

include("include/db_connect.php");
$id = $_GET["id"];

if (isset($_POST["submit_edit"]))
{
   if ($_SESSION['auth_admin_login'] == 'admin')
   {

    $error = array();

    if (!$_POST["admin_login"]) $error[] = "Логин занят!";
    if ($_POST["admin_pass"])
    {
    $pass   = $_POST["admin_pass"];
    /*$pass   = strrev($pass);
    $pass   = "pass='".strtolower("mb03foo51".$pass."qj2jjdp9")."',";*/
    }
 if (!$_POST["admin_fio"]) $error[] = "Укажите ФИО!";
    if (!$_POST["admin_role"]) $error[] = "Укажите должность!";
    if (!$_POST["admin_email"]) $error[] = "Укажите E-mail!";

 if (count($error))
 {
    $_SESSION['message'] = "<p id='form-error'>".implode('<br />',$error)."</p>";
 }else
 {
     if ($_POST["add_tovar"])
       {
          $add_tovar = "1";
       }else { $add_tovar = "0"; }

       if ($_POST["edit_tovar"])
       {
          $edit_tovar = "1";
       }else { $edit_tovar = "0"; }

       if ($_POST["delete_tovar"])
       {
          $delete_tovar= "1";
       }else { $delete_tovar = "0"; }

        if ($_POST["accept_reviews"])
       {
          $accept_reviews= "1";
       }else { $accept_reviews = "0"; }

       if ($_POST["delete_reviews"])
       {
          $delete_reviews= "1";
       }else { $delete_reviews = "0"; }

       if ($_POST["view_admin"])
       {
          $view_admin= "1";
       }else { $view_admin = "0"; }

       mysqli_query($link,"UPDATE reg_admin SET login='$_POST[admin_login]', pass='$_POST[admin_pass]', fio='$_POST[admin_fio]', role='$_POST[admin_role]', email='$_POST[admin_email]', phone='$_POST[admin_phone]', add_tovar='$add_tovar', edit_tovar=$edit_tovar, delete_tovar=$delete_tovar, accept_reviews=$accept_reviews, delete_reviews=$delete_reviews, view_admin=$view_admin  WHERE id = '$_GET[id]'");


       $_SESSION['message'] = "<p id='form-success'>Пользователь успешно изменён!</p>";
 }

   } else
   {
     $msgerror = 'У вас нет прав на изменение администраторов!';
   }
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="jquery_confirm/jquery_confirm.js"></script>
	<title>Панель Управления - Изменение Администратора</title>
</head>
<body>
<div id="block-body">
<?php
	include("include/block-header.php");
?>
<div id="block-content">
<div id="block-parameters">
<p id="title-page" >Изменение Администратора</p>
</div>
<?php
if (isset($msgerror)) echo '<p id="form-error" align="center">'.$msgerror.'</p>';

	if(isset($_SESSION['message']))
  {
	echo $_SESSION['message'];
	unset($_SESSION['message']);
  }

$result = mysqli_query($link,"SELECT * FROM reg_admin WHERE id='$id'");

 If (mysqli_num_rows($result) > 0)
{
$row = mysqli_fetch_array($result);
do
{

if ($row["add_tovar"] == "1") $add_tovar = "checked";
if ($row["edit_tovar"] == "1") $edit_tovar = "checked";
if ($row["delete_tovar"] == "1") $delete_tovar = "checked";
if ($row["accept_reviews"] == "1") $accept_reviews = "checked";
if ($row["delete_reviews"] == "1") $delete_reviews = "checked";
if ($row["view_admin"] == "1") $view_admin = "checked";

echo '
<form method="post" id="form-info" >

<ul id="info-admin">
<li><label>Логин</label><input type="text" name="admin_login"  value="'.$row["login"].'"/></li>
<li><label>Пароль</label><input type="password" name="admin_pass" value="'.$row["pass"].'" /></li>
<li><label>ФИО</label><input type="text" name="admin_fio" value="'.$row["fio"].'" /></li>
<li><label>Должность</label><input type="text" name="admin_role" value="'.$row["role"].'" /></li>
<li><label>E-mail</label><input type="text" name="admin_email" value="'.$row["email"].'" /></li>
<li><label>Телефон</label><input type="text" name="admin_phone" value="'.$row["phone"].'" /></li>
</ul>

<h3 id="title-privilege" >Привилегии</h3>

<p id="link-privilege"><a id="select-all" >Выбрать все</a> | <a id="remove-all" >Снять все</a></p>

<div class="block-privilege">


<ul class="privilege">
<li><h3>Товары</h3></li>

<li>
<input type="checkbox" name="add_tovar" id="add_tovar" value="1" '.$add_tovar.' />
<label for="add_tovar">Добавление товаров.</label>
</li>

<li>
<input type="checkbox" name="edit_tovar" id="edit_tovar" value="1" '.$edit_tovar.' />
<label for="edit_tovar">Изменение товаров.</label>
</li>

<li>
<input type="checkbox" name="delete_tovar" id="delete_tovar" value="1" '.$delete_tovar.' />
<label for="delete_tovar">Удаление товаров.</label>
</li>

</ul>

<ul class="privilege">
<li><h3>Отзывы</h3></li>

<li>
<input type="checkbox" name="accept_reviews" id="accept_reviews" value="1" '.$accept_reviews.' />
<label for="accept_reviews">Модерация отзывов.</label>
</li>

<li>
<input type="checkbox" name="delete_reviews" id="delete_reviews" value="1" '.$delete_reviews.' />
<label for="delete_reviews">Удаление отзывов.</label>
</li>

</ul>

</div>

<div class="block-privilege">

<ul class="privilege">
<li><h3>Администраторы</h3></li>

<li>
<input type="checkbox" name="view_admin" id="view_admin" value="1" '.$view_admin.' />
<label for="view_admin">Просмотр администраторов.</label>
</li>

</ul>

</div>

<p align="right"><input type="submit" id="submit_form" name="submit_edit" value="Сохранить"/></p>
</form>

';

}
  while($row = mysqli_fetch_array($result));
}

?>

</div>
</div>
</body>
</html>
<?php
}else
{
    header("Location: login.php");
}?>