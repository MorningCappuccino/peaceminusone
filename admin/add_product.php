<?php
session_start();
if ($_SESSION['auth_admin'] == "yes_auth")
{

       if (isset($_GET["logout"]))
    {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }

  $_SESSION['urlpage'] = "<a href='index.php' >Главная</a> \ <a href='tovar.php' >Косметика</a> \ <a>Добавление товара</a>";

  include("include/db_connect.php");

    if ($_POST["submit_add"])
    {
 if ($_SESSION['add_tovar'] == '1')
 {

      $error = array();

    // Проверка полей

       if (!$_POST["form_title"])
      {
         $error[] = "Укажите название товара";
      }

       if (!$_POST["form_price"])
      {
         $error[] = "Укажите цену";
      }

      /*else
      {
        $result = mysqli_query($link,"SELECT * FROM type_product WHERE id='{$_POST["form_category"]}'");
        $row = mysqli_fetch_array($result);
        $selectbrand = $row["type"];

      }*/

 // Проверка чекбоксов

       if ($_POST["chk_visible"])
       {
          $chk_visible = "1";
       }else { $chk_visible = "0"; }

       if ($_POST["chk_new"])
       {
          $chk_new = "1";
       }else { $chk_new = "0"; }

       if ($_POST["chk_leader"])
       {
          $chk_leader= "1";
       }else { $chk_leader = "0"; }


       if (count($error))
       {
            $_SESSION['message'] = "<p id='form-error'>".implode('<br />',$error)."</p>";

       }else
       {
              $date = date("Y-m-d");
              $sale=0;
              $count=0;
                  mysqli_query($link,"INSERT INTO products( product_id,title,price,id_brand,mini_description,image, description, `datetime`,new,leader,sale,visible,count,id_type)
            VALUES(
                            NULL,
                            '".$_POST["form_title"]."',
                            '".$_POST["form_price"]."',
                            '".$_POST["form_brand"]."',
                            '".$_POST["form_seo_description"]."',
                            '".$_POST["upload_image"]."',
                            '".$_POST["txt2"]."',
                            '".$date."',
                            '".$chk_new."',
                            '".$chk_leader."',
                            '".$sale."',
                            '".$chk_visible."',
                            '".$count."',
                            '".$_POST["form_type"]."'
            )");
            $id=mysqli_insert_id($link);
      $_SESSION['message'] = "<p id='form-success'>Товар успешно добавлен!</p>";
}
 }else
 {
   $msgerror = 'У вас нет прав на добавление товаров!';
 }
}

?>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="./ckeditor/ckeditor.js"></script>
  <title>Панель Управления</title>
</head>
<body>
<div id="block-body">
<?php
  include("include/block-header.php");
?>
<div id="block-content">
<div id="block-parameters">
<p id="title-page" >Добавление товара</p>
</div>
<?php
if (isset($msgerror)) echo '<p id="form-error" align="center">'.$msgerror.'</p>';

     if(isset($_SESSION['message']))
    {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
    }

     if(isset($_SESSION['answer']))
    {
    echo $_SESSION['answer'];
    unset($_SESSION['answer']);
    }
?>

<form enctype="multipart/form-data" method="post">
<ul id="edit-tovar">

<li>
<label>Название товара</label>
<input type="text" name="form_title" />
</li>

<li>
<label>Цена</label>
<input type="text" name="form_price"  />
</li>

<li>
<label>Бренд</label>
<select name="form_brand" id="brand" size="1" >
<option value="1" >MIZON (Южная Корея)</option>
<option value="2" >PRRETI (Южная Корея)</option>
<option value="3" >LEBELAGE (Южная Корея)</option>
<option value="4" >KUAN IM (Таиланд)</option>
<option value="5" >SEANTREE (Южная Корея)</option>
<option value="6" >ABHAIPHUBEJHR (Таиланд)</option>
<option value="7" >A`PIEU (Южная Корея)</option>
<option value="8" >ELIZAVECCA (Южная Корея)</option>
<option value="9" >HELLO BEAUTY (Южная Корея)</option>
</select>
</li>

<li>
<label>Краткое описание</label>
<textarea name="form_seo_description"></textarea>
</li>

<li>
<label>Тип товара</label>
<select name="form_type" id="type" size="1" >

<option value="1" >Крема для лица</option>
<option value="2" >Маски для лица</option>
<option value="3" >Сыворотки для лица</option>
<option value="4" >Патчи для глаз</option>
</select>
</li>

</ul>
<label class="stylelabel" >Картинка</label>

<div id="baseimg-upload">
<input type="hidden" name="MAX_FILE_SIZE" value="5000000"/>
<input type="file" name="upload_image" />
<?php
$error_img = array();

if($_FILES['upload_image']['error'] > 0)
{
//в зависимости от номера ошибки выводим соответствующее сообщение
 switch ($_FILES['upload_image']['error'])
 {
 case 1: $error_img[] =  'Размер файла превышает допустимое значение UPLOAD_MAX_FILE_SIZE'; break;
 case 2: $error_img[] =  'Размер файла превышает допустимое значение MAX_FILE_SIZE'; break;
 case 3: $error_img[] =  'Не удалось загрузить часть файла'; break;
 case 4: $error_img[] =  'Файл не был загружен'; break;
 case 6: $error_img[] =  'Отсутствует временная папка.'; break;
 case 7: $error_img[] =  'Не удалось записать файл на диск.'; break;
 case 8: $error_img[] =  'PHP-расширение остановило загрузку файла.'; break;
 }
}
else
{
//проверяем расширения
if($_FILES['upload_image']['type'] == 'image/jpeg' || $_FILES['upload_image']['type'] == 'image/jpg' || $_FILES['upload_image']['type'] == 'image/png')
{

$imgext = strtolower(preg_replace("#.+\.([a-z]+)$#i", "$1", $_FILES['upload_image']['name']));

   //папка для загрузки
$uploaddir = '../images/bd/';
//новое сгенерированное имя файла
$newfilename = $_POST["form_type"].'-'.$id.rand(10,100).'.'.$imgext;
//путь к файлу (папка.файл)
$uploadfile = $uploaddir.$newfilename;

//загружаем файл move_uploaded_file
if (move_uploaded_file($_FILES['upload_image']['tmp_name'], $uploadfile))
{

  $update = mysqli_query($link, "UPDATE products SET image='$newfilename' WHERE product_id= $id");

}
else
{
 $error_img[] =  "Ошибка загрузки файла.";
}
}
else
{
 $error_img[] =  'Допустимые расширения: jpeg, jpg, png';
}
}
?>

</div>


<h3 class="h3click" >Описание товара</h3>
<div class="div-editor2" >
<textarea id="editor2" name="txt2" cols="100" rows="20"></textarea>
    <script type="text/javascript">
      var ckeditor1 = CKEDITOR.replace( "editor2" );
      AjexFileManager.init({
        returnTo: "ckeditor",
        editor: ckeditor1
      });
    </script>
 </div>

<h3 class="h3title" >Настройки товара</h3>
<ul id="chkbox">
<li><input type="checkbox" name="chk_new" id="chk_new"  /><label for="chk_new" >Новый товар</label></li>
<li><input type="checkbox" name="chk_leader" id="chk_leader"  /><label for="chk_leader" >Популярный товар</label></li>
<li><input type="checkbox" name="chk_visible" id="chk_visible" /><label for="chk_visible" >Показывать товар</label></li>

</ul>


    <p align="right" ><input type="submit" id="submit_form" name="submit_add" value="Добавить товар"/></p>
</form>

</div>
</div>

</body>
</html>
<?php
}else
{
    header("Location: login.php");
}
?>